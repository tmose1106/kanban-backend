import base64

from aiohttp import web
from aiohttp_session import setup as setup_session, get_session, new_session, SimpleCookieStorage


routes = web.RouteTableDef()


@routes.get('/')
async def hello(request):
    return web.Response(text='Hello, World!')


@routes.post('/login')
async def auth(request):
    content = await request.json()

    try:
        username, password = content['username'], content['password']
    except KeyError:
        raise web.HTTPBadRequest(reason='Missing username or password')

    if username in request.app['credentials']:
        if password == request.app['credentials'][username]:
            session = await new_session(request)

            session['user_id'] = list(request.app['credentials'].keys()).index(username)

            return web.Response(text=f'You have logged in as {username}')
    
    raise web.HTTPBadRequest(reason='Username or password was incorrect')


@routes.post('/logout')
async def logout(request):
    session = await get_session(request)
 
    if session.new:
        raise web.HTTPForbidden(reason='User was not logged in')

    session.invalidate()

    return web.Response(text='User was logged out')


@routes.get('/secure')
async def secure(request):
    session = await get_session(request)

    if session.new or 'user_id' not in session:
        raise web.HTTPForbidden(reason='You do not have access to this endpoint')

    user_id = session['user_id']

    if user_id == 0:
        return web.Response(text='You are secured!')
    else:
        raise web.HTTPForbidden(reason='You do not have access to this endpoint')


def get_app():
    app = web.Application()

    app['credentials'] = {
        'tedm': 'psquiddy',
    }

    setup_session(app, SimpleCookieStorage(cookie_name='KANBAN_SESSION'))

    app.add_routes(routes)

    return app

