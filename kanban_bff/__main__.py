from aiohttp import web

from . import get_app


def cli():
    web.run_app(get_app())


if __name__ == '__main__':
    cli()

