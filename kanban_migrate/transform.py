from copy import deepcopy
from json import dumps, load, loads
from pathlib import Path
from pprint import pprint
from sys import argv, exit as sys_exit


DEFAULT_BOARD_STYLE = {
    'board': {
        'background': {},
        'backgroundImage': {},
        'foreground': {},
    },
    'column': {
        'background': {},
        'backgroundImage': {},
        'foreground': {},
        'border': {},
        'font': {},
    },
    'card': {
        'background': {},
        'backgroundImage': {},
        'foreground': {},
        'border': {},
        'font': {},
    },
}


def update_board_styles(board_data):
    ''' Add board and column styles '''
    try:
        board_style = board_data['style']
    except KeyError:
        board_data['style'] = deepcopy(DEFAULT_BOARD_STYLE)

    try:
        columns = board_data['columns']
    except KeyError:
        board_data['columns'] = []

    for column in board_data['columns']:
        try:
            _ = column['style']
        except KeyError:
            column['style'] = {
                'column': {
                    'background': {},
                    'backgroundImage': {},
                    'foreground': {},
                    'border': {},
                    'font': {},
                },
                'card': {
                    'background': {},
                    'backgroundImage': {},
                    'foreground': {},
                    'border': {},
                    'font': {},
                },
            }

    return board_data


DEFAULT_CARD_STYLE = {
    'background': {},
    'backgroundImage': {},
    'foreground': {},
    'border': {},
    'font': {},
}


def update_board_card_styles(board_data):
    try:
        columns = board_data['columns']
    except KeyError:
        print('No columns!')
        return board_data

    if not isinstance(columns, tuple):
        print('Not a list!', type(columns))
        return board_data

    for column in columns:
        print('HELLO')
        try:
            cards = column['cards']
        except KeyError:
            print('No cards!')
            continue
        
        if not isinstance(cards, tuple):
            print('Cards no a list!', type(cards))
            continue

        for card in cards:
            try:
                style = card['style']
            except KeyError:
                card['style'] = deepcopy(DEFAULT_CARD_STYLE)

                continue

            if not isinstance(style, dict):
                continue

            new_style = deepcopy(DEFAULT_CARD_STYLE)

            """
            'backgroundColor': '#ffffff',
            'backgroundOffset': 46,
            'backgroundUrl': 'https://images.uesp.net/c/ca/ON-concept-Prince-Azura-emblem.png',
            'borderColor': '#000000',
            'fontColor': '#ffffff',
            'foregroundColor': '#404040',
            'foregroundOpacity': 128
            """

            if 'backgroundColor' in style:
                new_style['background']['color'] = style['backgroundColor']

            if 'backgroundOpacity' in style:
                new_style['background']['opacity'] = style['backgroundOpacity']

            if 'backgroundOffset' in style:
                new_style['backgroundImage']['offset'] = style['backgroundOffset']

            if 'backgroundUrl' in style:
                new_style['backgroundImage']['url'] = style['backgroundUrl']

            if 'borderColor' in style:
                new_style['border']['color'] = style['borderColor']

            if 'borderOpacity' in style:
                new_style['border']['opacity'] = style['borderOpacity']

            if 'fontColor' in style:
                new_style['font']['color'] = style['fontColor']

            if 'fontOpacity' in style:
                new_style['font']['opacity'] = style['fontOpacity']

            if 'foregroundColor' in style:
                new_style['foreground']['color'] = style['foregroundColor']

            if 'foregroundOpacity' in style:
                new_style['foreground']['opacity'] = style['foregroundOpacity']

            card['style'] = new_style

    print('GGG', board_data)

    return board_data


commands = {
    'update-board-styles': update_board_styles,
    'update-board-card-styles': update_board_card_styles,
}

