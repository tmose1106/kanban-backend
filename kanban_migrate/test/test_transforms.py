from pprint import pprint

from kanban_migrate.transform import update_board_styles, update_board_card_styles


DEFAULT_BOARD_STYLE = {
    'board': {
        'background': {},
        'backgroundImage': {},
        'foreground': {},
    },
    'column': {
        'background': {},
        'backgroundImage': {},
        'foreground': {},
        'border': {},
        'font': {},
    },
    'card': {
        'background': {},
        'backgroundImage': {},
        'foreground': {},
        'border': {},
        'font': {},
    },
}

DEFAULT_COLUMN_STYLE = {
    'column': {
        'background': {},
        'backgroundImage': {},
        'foreground': {},
        'border': {},
        'font': {},
    },
    'card': {
        'background': {},
        'backgroundImage': {},
        'foreground': {},
        'border': {},
        'font': {},
    }, 
}

DEFAULT_CARD_STYLE = {
    'background': {},
    'backgroundImage': {},
    'foreground': {},
    'border': {},
    'font': {},
}

DEFAULT_CARD = {
    'title': '',
    'description': '',
    'style': DEFAULT_CARD_STYLE,
}


def get_card(title='', description='', style=DEFAULT_CARD_STYLE):
    if style is None:
        style = DEFAULT_CARD_STYLE

    return {
        'title': title,
        'description': description,
        'style': style,
    }


def get_column(*cards, title='', style=None):
    if style is None:
        style = DEFAULT_COLUMN_STYLE 

    return {
        'title': '',
        'cards': cards,
        'style': DEFAULT_COLUMN_STYLE,
    }


def get_board(*columns, title='', style=None):
    if style is None:
        style = DEFAULT_BOARD_STYLE

    return {
        'title': title,
        'columns': columns,
        'style': style,
    }


def test_update_board_styles_on_default():
    ''' Nothing should change from the default '''
    input_board = get_board()

    result_board = update_board_styles(input_board)

    assert(input_board == result_board)


def test_update_board_card_styles_on_board_with_partial_old_style_card():
    input_board = get_board(get_column(get_card(style={
        'backgroundColor': '#ffffff',
        'backgroundOffset': 46,
        'backgroundUrl': 'https://images.uesp.net/c/ca/ON-concept-Prince-Azura-emblem.png',
        'borderColor': '#000000',
        'fontColor': '#ffffff',
        'foregroundColor': '#404040',
        'foregroundOpacity': 128,
    })))

    result_board = update_board_card_styles(input_board)

    expected_board = get_board(get_column(get_card(style={
        'background': {
            'color': '#ffffff',
        },
        'backgroundImage': {
            'offset': 46,
            'url': 'https://images.uesp.net/c/ca/ON-concept-Prince-Azura-emblem.png',
        },
        'border': {
            'color': '#000000',
        },
        'font': {
            'color': '#ffffff',
        },
        'foreground': {
            'color': '#404040',
            'opacity': 128,
        },
    })))
 
    assert(result_board == expected_board)


def test_update_board_card_styles_on_board_with_multiple_cards():
    input_board = get_board(get_column(
        get_card(style={}),
        get_card(style={'background': {'color': '#000000', 'opacity': 255}}),
        get_card(style=DEFAULT_CARD_STYLE)))
 
    result_board = update_board_card_styles(input_board)

    assert(input_board == result_board)

