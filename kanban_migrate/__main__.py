from functools import partial
from json import dump, dumps, load, loads
from pathlib import Path
from pprint import pprint

import asyncio
import asyncpg
import click

from .transform import commands


async def select_boards(connection):
    results = await connection.fetch('SELECT id, data FROM public.boards ORDER BY public.boards.id')
    
    return {result['id']: result['data'] for result in results}


async def insert_boards(connection, boards):
    for board_id, board_data in boards.items():
        await connection.execute('UPDATE public.boards SET data = $2 WHERE public.boards.id = $1', board_id, dumps(board_data))


def read_boards(path):
    return {
        int(file_path.stem): file_path.read_text()
        for file_path in path.iterdir()
    }


def write_boards(path, boards):
    if not path.is_dir():
        path.mkdir()

    for board_id, board_data in boards.items():
        file_path = path.joinpath(f'{board_id}.json')

        file_path.write_text(board_data)


async def migrate_board_styles(connection, **kwargs):
    results = await connection.fetch('SELECT * FROM public.boards ORDER BY public.boards.id')

    if kwargs['log_changes_dir'] is not None:
        board_old_dir = kwargs['log_changes_dir'].joinpath('old')

        if not board_old_dir.is_dir():
            board_old_dir.mkdir()

        board_new_dir = kwargs['log_changes_dir'].joinpath('new')

        if not board_new_dir.is_dir():
            board_new_dir.mkdir()

    for result in results:
        board_data = loads(result['data'])

        print(result['id'], board_data['title'])

        board_id = result['id']

        if kwargs['log_changes_dir'] is not None:
            output_file_path = kwargs['log_changes_dir'].joinpath(f'old/{board_id}.json')

            output_file_path.write_text(dumps(board_data, indent=2))

        updated_board_data = update_board_styles(board_data)

        if kwargs['log_changes_dir'] is not None:
            output_file_path = kwargs['log_changes_dir'].joinpath(f'new/{board_id}.json')

            output_file_path.write_text(dumps(updated_board_data, indent=2))

        if not kwargs['dry_run']:
            await connection.execute('UPDATE public.boards SET data = $2 WHERE public.boards.id = $1', result['id'], dumps(updated_board_data))


@click.group()
def cli():
    pass


@cli.command()
def list_transforms():
    for function_name in commands:
        print(function_name)


@cli.command()
@click.argument('function', type=str)
@click.option('--dry-run', is_flag=True)
@click.option('--host', default='localhost')
@click.option('--list', 'list_', is_flag=True)
@click.option('--backup', type=Path)
def transform(function, backup, dry_run, host):
    if not path.is_dir():
        path.mkdir()

    try:
        transform_function = commands[function]
    except KeyError:
        sys_exit(f'Invalid transform function "{function}" requested')

    async def run():
        connection = await asyncpg.connect(
            host=host,
            database='postgres',
            user='postgres'
        )

        try:
            async with connection.transaction():
                boards = await select_boards(connection)

                write_boards(path.joinpath('before'), boards)

                transformed_boards = {
                    board_id: transform_function(board_data)
                    for board_id, board_data in boards.items()
                }

                write_boards(path.joinpath('after'), transformed_boards)

                if not dry_run:
                    await update_boards(connection, transformed_boards)
        finally:
            await connection.close()

    asyncio.run(run())


@cli.command()
@click.argument('path', type=Path)
@click.option('--host', default='localhost')
@click.option('--force', is_flag=True)
def copy(path, host, force):
    ''' Copy boards in DB to files '''
    if not path.is_dir():
        path.mkdir(parents=True)

    async def run():
        connection = await asyncpg.connect(
            host=host,
            database='postgres',
            user='postgres'
        )

        try:
            boards = await select_boards(connection)

            write_boards(path, boards)
        finally:
            await connection.close()

    asyncio.run(run())


@cli.command()
@click.argument('path', type=Path)
@click.option('--host', default='localhost')
def update(path, host):
    ''' Write content of files containing board data to DB by ID '''
    if not path.is_dir():
        sys_exit(f'Path "{path}" does not exist')

    boards = read_boards(path)

    async def run():
        connection = await asyncpg.connect(
            host=host,
            database='postgres',
            user='postgres'
        )

        update_query = '''
            UPDATE public."boards"
            SET "data" = $2
            WHERE id = $1;'''

        try:
            for board_id, board_data in boards.items():
                await connection.execute(update_query, board_id, board_data)
        finally:
            await connection.close()

    asyncio.run(run())


@cli.command()
@click.argument('input_path', type=Path)
@click.argument('function_name', type=str)
@click.option('--output-path', type=Path)
def file(input_path, output_path, function_name):
    try:
        function = commands[function_name]
    except KeyError:
        sys_exit(f'Function {function_name} does not exist')

    input_data = load(input_path.open('r', encoding='utf-8'))

    output_data = function(input_data)

    if output_path is None:
        print(dumps(output_data, indent=2))
    else:
        dump(output_data, output_path.open('w', encoding='utf-8'), indent=2)

