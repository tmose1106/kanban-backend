from collections import namedtuple
from cryptography.fernet import Fernet
from datetime import date, datetime
from functools import partial, wraps
from hashlib import pbkdf2_hmac
from json import dumps, loads
from os import urandom
from random import choices, randint
from string import hexdigits

from asyncpg import create_pool, exceptions
from mnemonicode import mnformat
from starlette.applications import Starlette
from starlette.endpoints import HTTPEndpoint
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import HTMLResponse, JSONResponse, PlainTextResponse
from starlette.routing import Route

from .queries import queries


SESSION_COOKIE_KEY = 'kanban_session'

DEFAULT_PAGE = '''<!DOCTYPE html>
<html>
    <head>
        <title>Kanban API</title>
    </head>
    <body>
        <h1>Kanban API</h1>
        <p>This is the API for kanban-board</p>
        <dl>
        {}
        </dl>
    </body>
</html>
'''


class JSONResponseWithDates(JSONResponse):
    @staticmethod
    def _serialize(obj):
        """JSON serializer for objects not serializable by default json code"""

        if isinstance(obj, (datetime, date)):
            return obj.isoformat()

        raise TypeError ("Type %s not serializable" % type(obj))

    def render(self, content):
        return dumps(content, default=self._serialize).encode('ascii')


async def index(request):
    return PlainTextResponse('Beware: Things will be broken!!!')


async def api_documentation(request):
    ''' Provides API documentation (This page) '''
    route_descriptions = [
        (
            route.path,
            route.endpoint.__doc__.strip() if route.endpoint.__doc__ is not None else '',
            ' '.join(route.methods)
        )
        for route in request.app.routes
    ]

    description_elements = [
        f'<dt>{path} ({methods})</dt>\n<dd>{description}</dd>\n'
        for path, description, methods in route_descriptions
    ]

    return HTMLResponse(DEFAULT_PAGE.format(''.join(description_elements)))


async def register(request):
    ''' Register a new user '''
    body = await request.json()

    try:
        name, password = body['username'], body['password']
    except KeyError:
        return PlainTextResponse('Username or password not provided', 400)

    new_salt = urandom(64)

    hashed_password = pbkdf2_hmac('sha256', password.encode('ascii'), new_salt, 100000)

    number = str(randint(0, 9999)).zfill(4)

    query = queries['insert_user'].get_sql()

    async with request.app.state.database.acquire() as connection:
        await connection.execute(query, name, number, hashed_password, new_salt)

        return PlainTextResponse('User {username} was created', 200)


async def login(request):
    ''' Log in an existing user '''
    body = await request.json()

    try:
        username, password = body['username'], body['password']
    except KeyError:
        return PlainTextResponse('Username or password not provided', 400)

    salt_query = queries['get_user_salt'].get_sql()
    id_query = queries['get_user_identification'].get_sql()

    async with request.app.state.database.acquire() as connection:
        salt = await connection.fetchval(salt_query, username)

        if salt is None:
            return PlainTextResponse(f'Invalid username or password', 401)

        hashed_password = pbkdf2_hmac('sha256', password.encode('ascii'), salt, 100000)

        user_identificaiton = await connection.fetchrow(id_query, username, hashed_password)

        if (user_identificaiton is None):
            return PlainTextResponse(f'Invalid username or password', 401)

        user_data = dict(user_identificaiton)

    if user_data is not None:
        response = PlainTextResponse(f'User {username} has been logged in', 200)

        session = {
            f'user_{key}': value
            for key, value in user_identificaiton.items()
        }

        encrypted_session = request.app.state.key.encrypt(dumps(session).encode('utf-8'))

        response.set_cookie(SESSION_COOKIE_KEY, encrypted_session.decode('utf-8'), expires=10_800, secure=True, httponly=True, samesite='strict')

        return response
    else:
        return PlainTextResponse(f'Invalid username or password', 401)


async def logout(request):
    ''' Log a user out from their session by invalidating cookie ''' 
    session_json = request.cookies.get(SESSION_COOKIE_KEY)

    if session_json is None:
        return PlainTextResponse('You do not have an open session', 403)

    response = PlainTextResponse('You have been logged out successfully', 200)

    response.delete_cookie(SESSION_COOKIE_KEY)

    return response


''' Endpoint method decorators '''

User = namedtuple('User', ['name', 'number'])


def params_user(func):
    @wraps(func)
    async def func(*args):
        request = args[1] if len(args) == 2 else args[0]

        user_name = request.path_params['user_name']
        user_number = request.path_params['user_number']

        user = User(user_name, user_number)

        request.state.params_user = user

        return await func(*args)

    return func


def decrypt_session_data(key, encrypted_cookie_data): 
    decrypted_cookie_data = key.decrypt(encrypted_cookie_data.encode('utf-8'))

    return loads(decrypted_cookie_data.decode('utf-8'))


def session(func):
    @wraps(func)
    async def inner_session_func(*args):
        request = args[1] if len(args) == 2 else args[0]

        encrypted_cookie_data = request.cookies.get(SESSION_COOKIE_KEY)

        if encrypted_cookie_data is None:
            return PlainTextResponse('You don\'t have a session, and cannot access this endpoint', 403)

        session_data = decrypt_session_data(request.app.state.key, encrypted_cookie_data)

        request.state.session = session_data

        return await func(*args)

    return inner_session_func


def session_user(func):
    @wraps(func)
    async def inner_func(*args):
        request = args[1] if len(args) == 2 else args[0]

        try:
            session = request.state.session
        except KeyError:
            return PlainTextResponse('Session is missing on request', 500)

        try:
            user_name = session['user_name']
        except KeyError:
            return PlainTextResponse('Session is missing user name', 422)

        try:
            user_number = session['user_number']
        except KeyError:
            return PlainTextResponse('Session is missing user number', 422)

        user = User(user_name, user_number)

        request.state.session_user = user

        return await func(*args)

    return inner_func


@session
@session_user
async def secure(request):
    ''' A protected endpoint that checks the user's session '''
    return PlainTextResponse('You have a session and can access this endpoint', 200)


class BoardEndpoint(HTTPEndpoint):
    @staticmethod
    async def get(request):
        board_name = request.path_params['board_name']
 
        board_query = queries['get_board_by_name'].get_sql()
        contributor_query = queries['get_board_contributors'].get_sql()

        async with request.app.state.database.acquire() as connection:
            board_result = await connection.fetchrow(board_query, board_name)

            if board_result is None:
                return PlainTextResponse('You do not have access to this board', 401) 

            board = dict(board_result)

            contributor_results = await connection.fetch(contributor_query, board['id'])

            editors = {
                result['id']: not result['view_only']
                for result in contributor_results
            }

        encrypted_cookie_data = request.cookies.get(SESSION_COOKIE_KEY)

        if encrypted_cookie_data is None:
            user_id = -1
        else:
            session = decrypt_session_data(request.app.state.key, encrypted_cookie_data)

            user_id = session['user_id']

        if not board['public'] and user_id not in editors:
            return PlainTextResponse('You do not have access to this board', 401) 
        else:
            return JSONResponseWithDates({
                **board,
                'editable': user_id in editors and editors[user_id],
            })


    @staticmethod
    @session
    async def patch(request):
        board_name = request.path_params['board_name']

        id_query = queries['get_board_id_user_can_contribute_to'].get_sql()
        board_query = queries['get_board_by_id'].get_sql()
        update_query = queries['update_board_data'].get_sql()
        update_active_query = queries['update_board_active'].get_sql()

        user_id = request.state.session['user_id']

        body = await request.json()

        try:
            last_modified = datetime.fromisoformat(body['lastModified'])
        except KeyError:
            return PlainTextResponse('Must provide last modification time to update a board', 422)
        except ValueError:
            return PlainTextResponse('Last Modified datetime must be in iso format', 422)

        async with request.app.state.database.acquire() as connection:
            board_id = await connection.fetchval(id_query, user_id, board_name)

            if board_id is None:
                return PlainTextResponse('You do not have write access to this board', 401)

            board = await connection.fetchrow(board_query, board_id)

            if board['modified_datetime'] != last_modified:
                return PlainTextResponse('The board has been updated since last fetch, can\'t write', 409)

            if 'active' in body:
                active = body['active']

                await connection.execute(update_active_query, board_id, active, datetime.utcnow())

                status = 'activated' if active else 'deactivated'

                return PlainTextResponse(f'Board {board_name} has been {status}')
            elif 'data' in body:
                board_data = body['data']

                await connection.execute(update_query, board_id, dumps(board_data), datetime.utcnow())

                return PlainTextResponse('The board has been updated!')
            else:
                return PlainTextResponse('No board data was provided for updating.')


class BoardContributorsEndpoint(HTTPEndpoint):
    @staticmethod
    @session
    async def get(request):
        board_name = request.path_params['board_name']

        contributor_query = queries['get_contributors_for_accessible_board'].get_sql()

        user_id = request.state.session['user_id']

        async with request.app.state.database.acquire() as connection:
            results = await connection.fetch(contributor_query, user_id, board_name)

            return JSONResponse([dict(result) for result in results])

    @staticmethod
    @session
    async def post(request):
        board_name = request.path_params['board_name']

        user_id_query = queries['get_user_id_by_name_and_number'].get_sql()

        board_id_query = queries['get_editable_board_id'].get_sql()

        insert_contributor_query = queries['insert_user_as_contributor'].get_sql()

        user_id = request.state.session['user_id']

        body = await request.json()

        try:
            user_name, user_number = body['user_name'], body['user_number']
        except KeyError:
            return PlainTextResponse('Valid username and/or number not provided')

        try:
            view_only = body['view_only']
        except KeyError:
            view_only = True

        async with request.app.state.database.acquire() as connection:
            other_user_id = await connection.fetchval(user_id_query, user_name, user_number)

            if other_user_id is None:
                return PlainTextResponse(f'User {user_name}#{user_number} not found')

            board_id = await connection.fetchval(board_id_query, user_id, board_name)

            if board_id is None:
                return PlainTextResponse('Could not find board that you could edit')

            try:
                await connection.execute(insert_contributor_query, other_user_id, board_id, view_only)
            except exceptions.UniqueViolationError:
                return PlainTextResponse(f'User {user_name}#{user_number} already is a contributor')

            return PlainTextResponse(f'User {user_name}#{user_number} added as contributor')

    @staticmethod
    @session
    async def delete(request):
        board_name = request.path_params['board_name']

        board_id_query = queries['get_editable_board_id'].get_sql()
        delete_contributor_query = queries['delete_contributor_by_ids'].get_sql()

        print(delete_contributor_query)

        user_id = request.state.session['user_id']

        body = await request.json()

        try:
            other_user_id = body['user_id']
        except KeyError:
            return PlainTextResponse('Valid username and/or number not provided')

        async with request.app.state.database.acquire() as connection:
            board_id = await connection.fetchval(board_id_query, user_id, board_name)

            if board_id is None:
                return PlainTextResponse('Could not find board that you could edit')

            try:
                await connection.execute(delete_contributor_query, other_user_id, board_id)
            except exceptions.UniqueViolationError:
                return PlainTextResponse(f'User is not a contributor for this board')

            return PlainTextResponse(f'User removed as contributor')
       

class UserEndpoint(HTTPEndpoint):
    @staticmethod
    @session
    @session_user
    @params_user
    async def get(request):
        current_user, target_user = request.state.session_user, request.state.params_user

        query = queries['get_user_info'].get_sql()

        async with request.app.state.database.acquire() as connection:
            result = await connection.fetchrow(query, target_user.name, target_user.number)

            user_data = dict(result)

        return JSONResponse({
            **user_data,
            # Suggest to the frontend whether the user can be edited
            # This will still be validated by the backend for fraud
            'editable': target_user == current_user
        })


@session
@session_user
async def get_current_user(request):
    user = request.state.session_user
    
    return JSONResponse({'user_name': user.name, 'user_number': user.number})


class UserBoardsEndpoint(HTTPEndpoint):
    @staticmethod
    @session
    async def get(request):
        ''' Get boards that the user has created or contributes to '''
        user_id = request.state.session['user_id']

        query = queries['get_boards_user_contributes_to'].get_sql()

        async with request.app.state.database.acquire() as connection:
            results = await connection.fetch(query, user_id)

            return JSONResponseWithDates([dict(result) for result in results])

    @staticmethod
    def _get_board_key():
        return '{}-{}'.format(mnformat(urandom(4)), ''.join(choices(hexdigits, k=8)))

    @staticmethod
    @session
    async def post(request):
        ''' Create a new board with the user as the contributor '''

        board_query = queries['insert_new_board'].get_sql()
        contributor_query = queries['insert_user_as_contributor'].get_sql()

        user_id = request.state.session['user_id']

        name = UserBoardsEndpoint._get_board_key()

        body = await request.json()

        data, public = body['board'], body['public']

        async with request.app.state.database.acquire() as connection:
            async with connection.transaction():
                result = await connection.fetchrow(board_query, name, dumps(data), datetime.utcnow(), datetime.utcnow(), public)

                await connection.execute(contributor_query, user_id, result['id'], False)

                return JSONResponse({'name': result['name']})


def get_connection_pool_callbacks(host, app):
    pool = None

    async def connect():
        pool = await create_pool(
            host=host,
            database='postgres',
            user='postgres'
        )

        app.state.database = pool

    async def disconnect():
        del app.state.database

        pool.close()

    return connect, disconnect


def get_app(secret_key, db_host='localhost', cors_origin=None):
    middleware = [Middleware(CORSMiddleware, 
        allow_origins=['http://localhost:5000', 'http://localhost:8000'],
        allow_methods=['*'],
        allow_headers=['*'],
        allow_credentials=True)]

    app = Starlette(
        debug=True,
        routes=[
            Route('/', index),
            # Route('/', api_documentation),
            Route('/board/{board_name:str}', BoardEndpoint),
            Route('/board/{board_name:str}/contributors', BoardContributorsEndpoint),
            Route('/user', get_current_user),
            Route('/user/boards', UserBoardsEndpoint),
            Route('/user/{user_name:str}#{user_number:int}', UserEndpoint),
    
            Route('/login', login, methods=['POST']),
            Route('/logout', logout, methods=['POST']),
            Route('/register', register, methods=['POST']),
            Route('/secure', secure),
        ],
        middleware = middleware
    )

    connect, disconnect = get_connection_pool_callbacks(db_host, app)

    app.add_event_handler('startup', connect)
    app.add_event_handler('shutdown', disconnect)

    app.state.key = Fernet(secret_key)

    return app


def get_dev_app(db_host):
    return get_app(b'2fg1X4L9Aeo4N_BaHswLNoltOFe_Az9QkItG4jwW6Io=', db_host)
