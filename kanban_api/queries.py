from pypika import AliasedQuery, PostgreSQLQuery as Query, Schema, Parameter


Public = Schema('public')

Boards = Public.boards
Contributors = Public.contributors
Users = Public.users


def get_boards_user_contributes_to_query():
    contributor_query = Query \
        .from_(Contributors) \
        .where(Contributors.user_id == Parameter('$1')) \
        .select(Contributors.board_id.as_('id'))

    board_query = Query \
        .with_(contributor_query, 'contributable_boards') \
        .from_(AliasedQuery('contributable_boards')) \
        .inner_join(Boards).using('id') \
        .where(Boards.active == True) \
        .select(Boards.id, Boards.name, Boards.creation_date, Boards.modified_datetime, Boards.public.as_('is_public'))

    return board_query


def get_board_id_user_can_contribute_to_query():
    contributor_query = Query \
        .from_(Contributors) \
        .select(Contributors.board_id.as_('id')) \
        .where(Contributors.user_id == Parameter('$1'))

    board_query = Query \
        .with_(contributor_query, 'contributable_boards') \
        .from_(AliasedQuery('contributable_boards')) \
        .inner_join(Boards).using('id') \
        .where(Boards.active == True) \
        .where(Boards.name == Parameter('$2')) \
        .select(Boards.id)

    return board_query


def get_accessible_board_id():
    ''' Get board ID where the user can view publicly or contribute in some way '''
    return Query \
        .from_(Boards) \
        .join(Contributors).on(Boards.id == Contributors.board_id) \
        .where(Boards.name == Parameter('$2')) \
        .where((Boards.public == True) | (Contributors.user_id == Parameter('$1'))) \
        .select(Boards.id)


def get_contributors_for_board():
    return Query \
        .from_(Contributors) \
        .join(Users).on(Users.id == Contributors.user_id) \
        .where(Contributors.board_id.isin(get_accessible_board_id())) \
        .select(Users.id, Users.name, Users.number, Contributors.view_only)


def get_editable_board_id():
    query = Query \
        .from_(Boards) \
        .join(Contributors).on(Boards.id == Contributors.board_id) \
        .where(Boards.name == Parameter('$2')) \
        .where(Contributors.user_id == Parameter('$1')) \
        .where(Contributors.view_only == False) \
        .select(Boards.id)

    return query


queries = {
    'get_users': Query.from_(Users)
        .select(Users.id, Users.name),
    'get_user_password': Query.from_(Users)
        .select(Users.password, Users.salt)
        .where(Users.name == Parameter('$1')),
    'get_user_salt': Query.from_(Users)
        .select(Users.salt)
        .where(Users.name == Parameter('$1')),
    'get_user_id': Query.from_(Users)
        .select(Users.id)
        .where(Users.name == Parameter('$1'))
        .where(Users.password == Parameter('$2')),
    'get_user_identification': Query.from_(Users)
        .select(Users.id, Users.name, Users.number)
        .where(Users.name == Parameter('$1'))
        .where(Users.password == Parameter('$2')),
    'insert_user': Query.into(Users)
        .columns(Users.name, Users.number, Users.password, Users.salt, Users.active)
        .insert(Parameter('$1'), Parameter('$2'), Parameter('$3'), Parameter('$4'), True),
    'get_boards_owned_by_user': Query.from_(Boards)
        .select(Boards.key, Boards.data)
        .where(Boards.user_id == Parameter('$1')),
    'get_board_keys_owned_by_user': Query.from_(Boards)
        .select(Boards.key)
        .where(Boards.user_id == Parameter('$1')),
    'insert_new_board_owned_by_user': Query.into(Boards)
        .columns(Boards.key, Boards.user_id, Boards.data, Boards.public)
        .insert(Parameter('$1'), Parameter('$2'), Parameter('$3'), Parameter('$4')),
    'get_board_owned_by_user': Query.from_(Boards)
        .select(Boards.key, Boards.user_id, Boards.data)
        .where(Boards.user_id == Parameter('$1'))
        .where(Boards.key == Parameter('$2')),
    'get_public_board': Query.from_(Boards)
        .select(Boards.key, Boards.user_id, Boards.data)
        .where(Boards.user_id == Parameter('$1'))
        .where(Boards.key == Parameter('$2')),
    # This needs to be updated when contributors are added
    'get_private_board': Query.from_(Boards)
        .select(Boards.key, Boards.user_id, Boards.data)
        .where(Boards.user_id == Parameter('$1'))
        .where(Boards.key == Parameter('$3'))
        .where((Parameter('$1') == Parameter('$2'))),
    # 'get_boards_user_contributes_to': get_boards_user_contributes_to_query(),
    'insert_new_board': Query.into(Boards)
        .columns(Boards.name, Boards.data, Boards.creation_date, Boards.modified_datetime, Boards.public, Boards.active)
        .insert(Parameter('$1'), Parameter('$2'), Parameter('$3'), Parameter('$4'), Parameter('$5'), True)
        .returning(Boards.id, Boards.name), 
    'insert_user_as_contributor': Query.into(Contributors)
        .columns(Contributors.user_id, Contributors.board_id, Contributors.view_only)
        .insert(Parameter('$1'), Parameter('$2'), Parameter('$3')),
    'get_board_by_name': Query.from_(Boards)
        .where(Boards.name == Parameter('$1'))
        .select('*'),
    'get_board_contributors': Query.from_(Contributors)
        .where(Contributors.board_id == Parameter('$1'))
        .select(Contributors.user_id.as_('id'), Contributors.view_only),
    'get_contributors_for_accessible_board': get_contributors_for_board(),
    'get_boards_user_contributes_to': get_boards_user_contributes_to_query(),
    'get_board_id_user_can_contribute_to': get_board_id_user_can_contribute_to_query(),
    'update_board_data': Query.update(Boards).set(Boards.data, Parameter('$2')).set(Boards.modified_datetime, Parameter('$3')).where(Boards.id == Parameter('$1')),
    'update_board_active': Query.update(Boards).set(Boards.active, Parameter('$2')).set(Boards.modified_datetime, Parameter('$3')).where(Boards.id == Parameter('$1')), 

    'get_editable_board_id': get_editable_board_id(),
    'get_user_id_by_name_and_number': Query
        .from_(Users)
        .where(Users.name == Parameter('$1'))
        .where(Users.number == Parameter('$2'))
        .select(Users.id),
    'delete_contributor_by_ids': Query
        .from_(Contributors)
        .delete()
        .where(Contributors.user_id == Parameter('$1'))
        .where(Contributors.board_id == Parameter('$2')),

    'get_board_by_id': Query.from_(Boards)
        .where(Boards.id == Parameter('$1'))
        .select('*'),
}

