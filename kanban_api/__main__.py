import logging
from pathlib import Path
from sys import exit as sys_exit

import click
from uvicorn import run as run_app

from . import get_app, get_dev_app


@click.group()
@click.help_option('-h', '--help')
@click.version_option('0.1.0', '-V', '--version')
def cli():
    pass


@cli.command()
@click.option('--host', default='localhost')
@click.help_option('-h', '--help')
def dev(host):
    run_app(get_dev_app(host), port=8081)


@cli.command()
@click.option('--secret', 'secret_path', type=Path, default=Path('secret.txt'))
@click.help_option('-h', '--help')
def prod(secret_path):
    if not secret_path.is_file():
        sys_exit(f'Path to secret key "{secret_path.absolute()}" is not a file')

    secret_key = secret_path.read_text()

    # log_level = logging.DEBUG if dev else logging.INFO

    # logging.basicConfig(level=log_level)

    run_app(get_app(secret_key), fd=0)


@cli.command()
def setup():
    from cryptography.fernet import Fernet

    secret_path = Path('secret.txt')

    secret_path.write_bytes(Fernet.generate_key())

    print(f'Wrote secret key to "{secret_path.absolute()}"')


if __name__ == '__main__':
    cli()

